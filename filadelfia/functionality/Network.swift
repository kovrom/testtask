//
//  Network.swift
//  filadelfia
//
//  Created by Роман Ковайкин on 16.10.2020.
//

import Foundation
import Alamofire

class Network{
    
    func getInflationPrice(amount: String){
        let url = "https://www.statbureau.org/calculate-inflation-price-json?country=russia&format=true&start=2020-10-01&end=2000-10-01&amount=1000"
       // var result: Array = [""]
        
        AF.request(url).responseJSON{ (response) in
            switch response.result{
            case .success(let JSON):
//
                print (JSON)
            return
            case .failure(let error):
                print(error)
            }
        }
        
  
    }
    
    func getGraphForPrice(amount: Float){
        let url = "https://www.statbureau.org/ru/russia/October-2020/October-2000/monthly-inflation-price-progression.png?width=635&height=280&overlay=Auto&chartType=SplineArea&amount=1000"
        
        AF.request(url).responseJSON{ (response) in
            switch response.result{
            case .success(let JSON):
                print (JSON)
            return
            case .failure(let error):
                print(error)
            }
        }
        
        
    }
    
}


