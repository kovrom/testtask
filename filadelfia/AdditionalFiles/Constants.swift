//
//  Constats.swift
//  filadelfia
//
//  Created by Роман Ковайкин on 16.10.2020.
//

import Foundation

class Constants{
    
    ///https://www.statbureau.org/{language}/{country}/monthly-inflation-current-year-mom.png?width={width}&height={height}&chartType={chartType}
    
    let linkForGraph = "https://www.statbureau.org/ru/russia/October-2020/October-2000/monthly-inflation-price-progression.png?width=635&height=280&overlay=Auto&chartType=SplineArea&"
    
    public let LINKFORPRICECHANGE = "https://www.statbureau.org/calculate-inflation-rate-json"
    
    //?country=russia&start=2015-10-01&end=2020-09-01
    
    ///https://www.statbureau.org/ru/russia/October-2020/October-2016/monthly-inflation-price-progression.png?width=635&height=280&overlay=Auto&chartType=SplineArea&amount=1000&denominationsToApply[0]=1998-01-01 - график
    ///
    ///
    ///https://www.statbureau.org/calculate-inflation-rate-json?country=russia&start=2015-10-01&end=2020-09-01 //размер инфляции
}
