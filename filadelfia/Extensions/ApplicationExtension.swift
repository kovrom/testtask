//
//  ApplicationExtension.swift
//  filadelfia
//
//  Created by Роман Ковайкин on 17.10.2020.
//

import Foundation
import UIKit

extension UIApplication{
    func endEditing(){
        sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
