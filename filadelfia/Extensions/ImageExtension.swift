//
//  ImageExtension.swift
//  filadelfia
//
//  Created by Роман Ковайкин on 17.10.2020.
//

import Foundation
import UIKit

extension UIImageView{
    func ladImageWithURL(urlString: String){
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: URL(string: urlString)!) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
