//
//  RegistrationViewController.swift
//  filadelfia
//
//  Created by Роман Ковайкин on 14.10.2020.
//

import SwiftUI


//MARK: TODO: tap to dismiss keyboard signUp Method

struct RegistrationView: View {
    let viewModel = AuthViewModel()
    @State private var email: String = ""
    @State private var password: String = ""
    @State private var confirmPassword: String = ""
    @State private var isError: Bool = false
    
    var body: some View {
        
        NavigationView {
            
            Form{
                Section{
                    TextField("Email", text: $email)
                        .keyboardType(.emailAddress)
                    SecureField("Пароль", text: $password)
                    SecureField("Подтвердите пароль", text: $confirmPassword) {
                        isError = true
                    }
                    .foregroundColor(isError ? Color.red : Color.black)
                    
                }
                
                Section{
                    ZStack{
                        Button("Зарегистрироваться") {
                            guard email != "" else{
                                print("error! email is nil")
                                return
                            }
                            guard password != "" else{
                                print("error! Password is nil or doesn't ")
                                return
                            }
                            guard confirmPassword != "", password == confirmPassword else{
                                print("Password != confirmPassword")
                                return
                            }
                            
                            viewModel.signUp(Email: email, password: password)
                        }
                        .foregroundColor(.black)
                    }.frame(maxWidth: .infinity,maxHeight: .infinity)
                    
                }
            }
            
            
        }
    }
}

struct RegistrationViewController_Previews: PreviewProvider {
    static var previews: some View {
        RegistrationView()
    }
}
