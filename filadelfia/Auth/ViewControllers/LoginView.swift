//
//  ContentView.swift
//  filadelfia
//
//  Created by Роман Ковайкин on 13.10.2020.
//

import SwiftUI
import Firebase


struct LoginView: View {
    let viewModel = AuthViewModel()
    @State private var email: String = ""
    @State private var password = ""
    @State private var isError: Bool = false
    @State private var isPresented: Bool = false
    
    var body: some View {
        
        NavigationView{
            
            Form{
                Section{
                    TextField("Email", text: $email)
                        .keyboardType(.emailAddress)
                        .autocapitalization(.none)
                    
                    SecureField("Password", text: $password)
                        .keyboardType(.default)
                        .autocapitalization(.none)
                    
                }
                Section{
                    ZStack{
                        Button("Войти") {
                            guard email != "" else{
                                print("error! email is nil")
                                return
                            }
                            guard password != "" else{
                                print("error! Password is nil or doesn't ")
                                return
                            }
                            viewModel.signIn(email: email, password: password)
                        }
                        
                    }.frame(maxWidth: .infinity,maxHeight: .infinity)
                }
                
                Section{
                    NavigationLink(
                        destination: RegistrationView(),
                        label: {
                            Text("Регистрация")
                                .font(.title3)
                        })
                }
                
            }
            .navigationBarTitleDisplayMode(.inline)
            .toolbar {
                ToolbarItem(placement: .principal) {
                    Text("Вход")
                        .font(.headline)
                    
                }
            }
        }
    }
    
    
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            LoginView()
        }
    }
}
