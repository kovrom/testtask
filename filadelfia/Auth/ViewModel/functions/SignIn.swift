//
//  SignIn.swift
//  filadelfia
//
//  Created by Роман Ковайкин on 15.10.2020.
//

import Foundation
import Firebase
import SwiftUI

class SignIn{
    
    func signIn(email: String, password: String){
        
        Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
            if let error = error {
                if (error._code == 17020){
                    
                    print("Нет интернет соединения")
                }else if error._code == 17011{
                    
                    print("Неверно введен Email или пароль")
                }
            }else{
                
                UserDefaults.standard.setValue(true, forKey: "isUserLoggedIn")
                UserDefaults.standard.synchronize()
                print("Success")
            }
        }
        
    }
}
