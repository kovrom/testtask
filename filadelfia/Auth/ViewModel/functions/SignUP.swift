//
//  SignUP.swift
//  filadelfia
//
//  Created by Роман Ковайкин on 15.10.2020.
//

import Foundation
import Firebase

class SignUp {
    
    public func signUp(email: String, password: String){
        
        Auth.auth().createUser(withEmail: email, password: password) { (result, error) in
            if let error = error{
                print(error.localizedDescription)
                return
            }
            guard let uid = result?.user.uid else {
                print("uid error")
                return
            }
            
            let values = ["Email": email]
            
            Database.database().reference().child("Users").child(uid).updateChildValues(values) { (error, ref) in
                if let error = error{
                    print("Failed to update data", error.localizedDescription)
                    return
                }
            }
        }
    }
    
}
