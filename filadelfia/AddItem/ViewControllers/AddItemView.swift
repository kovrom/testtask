//
//  AddItemView.swift
//  filadelfia
//
//  Created by Роман Ковайкин on 15.10.2020.
//

import SwiftUI
import Alamofire

struct AddItemView: View {
    let addItemViewModel = InsertItem()
    
    @State private var price: String = ""
    @State var priceAfterInflation: String = ""
    @State private var selectedCurrency: Currency = Currency.RUB
    @State private var itemName: String = ""
    @State private var image: Data? = nil
    @State var imageView:UIImage = UIImage()
    @State private var didEnterPrice: Bool = false
    
    
    var body: some View {
        
        NavigationView{
            
            Form{
                Section{
                    TextField("Название товара", text: $itemName)
                        .frame(height: 50)
                    TextField("Текущаю цена", text: $price) { (isEditing) in
                        
                    } onCommit: {
                        DispatchQueue.global(qos: .userInitiated).async {
                            getInflationPrice(amount: price)
                        }
                        didEnterPrice = true
                    }
                    .frame(height: 50)
                }
                
                Section{
                    Picker(selection: $selectedCurrency, label: Text("Выберите валюту")) {
                        Text("RUB").tag(Currency.RUB)
                        Text("USD").tag(Currency.USD)
                        Text("EUR").tag(Currency.EUR)
                    }
                }
                
                Section{
                    Text("Цена в 2000: \(priceAfterInflation)")
                    NavigationLink("График", destination: GraphView(url: "https://www.statbureau.org/ru/\(getCountry())/October-2020/October-2000/monthly-inflation-price-progression.png?width=300&height=300&overlay=Auto&chartType=SplineArea&amount=\(price)"))
                }
                
                
                Section{
                    Button("Сохранить цену") {
                        guard price != "" else{
                            print("price is nil")
                            return
                        }
                        
                        guard itemName != "" else{
                            print("item name is nil")
                            return
                        }
                        addItemViewModel.insert(itemName: itemName, itemPrice: Float(price)!)
                    }
                    Button("Очистить цену") {
                        price = ""
                        priceAfterInflation = ""
                        didEnterPrice = false
                    }
                    
                }
               
                
            }
            .navigationBarTitle(Text("Добавление товара"))
        }
    }
    func getCountry() -> String{
        switch selectedCurrency{
        case .EUR:
            return "european-union"
        case .RUB:
            return "russia"
        case .USD:
            return "united-states"
        }
    }
    
    func getInflationPrice(amount: String){
        let url = "https://www.statbureau.org/calculate-inflation-price-json?country=\(getCountry())&format=true&start=2020-10-01&end=2000-10-01&amount=\(amount)"
        
        AF.request(url).responseJSON{ (response) in
            switch response.result{
            case .success(let JSON):
                priceAfterInflation = JSON as! String
                return
            case .failure(let error):
                print(error)
            }
        }.resume()
    }
}

struct AddItemView_Previews: PreviewProvider {
    static var previews: some View {
        AddItemView()
    }
}
