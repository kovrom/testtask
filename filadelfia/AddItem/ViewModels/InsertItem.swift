//
//  InsertItem.swift
//  filadelfia
//
//  Created by Роман Ковайкин on 17.10.2020.
//

import Foundation
import Firebase

class InsertItem{
    
    func insert(itemName: String, itemPrice: Float){

        let values: [String : Any] = ["Name": itemName, "Current price": itemPrice]
        
        Database.database().reference().child("Items").child(itemName).updateChildValues(values) { (error, ref) in
            if let error = error{
                print("Failed to update data", error.localizedDescription)
                return
            }
            
        }
    }
    
}
