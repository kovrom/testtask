//
//  Item.swift
//  filadelfia
//
//  Created by Роман Ковайкин on 16.10.2020.
//

import Foundation


struct Item: Decodable{
    var name: String
}
