//
//  GraphView.swift
//  filadelfia
//
//  Created by Роман Ковайкин on 16.10.2020.
//

import SwiftUI

struct GraphView: View {
    
    var url: String
    
    init(url: String) {
        self.url = url
    }
    
    var body: some View {
        RemoteImage(url: url)
            .frame(width: 300, height: 300)
    }
}

struct GraphView_Previews: PreviewProvider {
    static var previews: some View {
        GraphView(url: "")
    }
}

